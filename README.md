# Ansible Project
Provisions an Apache/PHP Linux EC2 instance in AWS using an Ansible [playbook](https://gitlab.com/mburolla/ansible/blob/master/playbook.yml).

# Getting Started
- Clone this repo
- Setup prerequisites:
  - Ansible
  - Boto
  - AWS VPC with a public subnet
  - Update `playbook.yml` with your key, subnet, and security group
- Run playbook: `ansible-playbook playbook.yml`

# Sample Output
Your IP address will be different.

<img src="https://gitlab.com/mburolla/ansible/uploads/b381cb6834a4108217ab08a06e4bf73e/image.png" width=600px>
  
# Modules
- [EC2](https://docs.ansible.com/ansible/latest/modules/ec2_module.html)
- [Copy](https://docs.ansible.com/ansible/latest/modules/copy_module.html)
- [Service](https://docs.ansible.com/ansible/latest/modules/service_module.html)
- [Yum](https://docs.ansible.com/ansible/latest/modules/yum_module.html)
- [Pause](https://docs.ansible.com/ansible/latest/modules/pause_module.html)
- [Debug](https://docs.ansible.com/ansible/latest/modules/debug_module.html)
- [Add_Host](https://docs.ansible.com/ansible/latest/modules/add_host_module.html)
- [Shell](https://docs.ansible.com/ansible/latest/modules/shell_module.html)
- [Wait_for](https://docs.ansible.com/ansible/latest/modules/wait_for_module.html)

# Build Configuration
- Ansible 2.9.0
- Python 2.7.10
- VS Code 1.38.1
- MacOS 10.13.3
